# cc-processing-system v0.0.0



- [Card](#card)
	- [Create card](#create-card)
	- [Retrieve card](#retrieve-card)
	- [Retrieve cards](#retrieve-cards)
	- [recharge the card](#recharge-the-card)
	- [credit from the card](#credit-from-the-card)
	


# Card

## Create card



	POST /cards


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| name			| 			|  <p>Card holder name.</p>							|
| cNumber			| 			|  <p>Card Number.</p>							|
| limit			| 			|  <p>Card credit limit.</p>							|
| balance			| 			|  <p>Card balance.</p>							|

## Retrieve card



	GET /cards/:id


## Retrieve cards



	GET /cards


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| q			| String			| **optional** <p>Query to search.</p>							|
| page			| Number			| **optional** <p>Page number.</p>							|
| limit			| Number			| **optional** <p>Amount of returned items.</p>							|
| sort			| String[]			| **optional** <p>Order of returned items.</p>							|
| fields			| String[]			| **optional** <p>Fields to be returned.</p>							|

## recharge the card



	PATCH /cards/:id/charge


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| amount			| 			|  <p>charging anount.</p>							|

## credit from the card



	PATCH /cards/:id/credit


### Parameters

| Name    | Type      | Description                          |
|---------|-----------|--------------------------------------|
| amount			| 			|  <p>current credit amount.</p>							|


