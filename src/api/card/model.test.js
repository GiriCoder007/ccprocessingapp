import { Card } from '.'

let card

beforeEach(async () => {
  card = await Card.create({ name: 'test', cNumber: 'test', limit: 'test', balance: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = card.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(card.id)
    expect(view.name).toBe(card.name)
    expect(view.cNumber).toBe(card.cNumber)
    expect(view.limit).toBe(card.limit)
    expect(view.balance).toBe(card.balance)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = card.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(card.id)
    expect(view.name).toBe(card.name)
    expect(view.cNumber).toBe(card.cNumber)
    expect(view.limit).toBe(card.limit)
    expect(view.balance).toBe(card.balance)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
