import { success, notFound, isValidCard, invalidCharge } from '../../services/response/'
import { Card } from '.'

export const create = ({ bodymen: { body } }, res, next) => {
  if (isValidCard(res)(body)) 
    Card.create(body)
    .then((card) => card.view(true))
    .then(success(res, 201))
    .catch(next)
}

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  Card.count(query)
    .then(count => Card.find(query, select, cursor)
      .then((cards) => ({
        count,
        rows: cards.map((card) => card.view())
      }))
    )
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  Card.findById(params.id)
    .then(notFound(res))
    .then((card) => card ? card.view() : null)
    .then(success(res))
    .catch(next)

export const charge = ({ bodymen: { body }, params }, res, next) =>
  Card.findById(params.id)
    .then(notFound(res))
    .then(invalidCharge(body, res))
    .then((card) => {
      card.balance += body.amount;
      card.save()
      .then((card) => card ? card.view(true) : null)
      .then(success(res))
      .catch(next)
    })
    
export const credit = ({ bodymen: { body }, params }, res, next) =>
    Card.findById(params.id)
      .then(notFound(res))   
      .then((card) => {
        card.balance -= body.amount;
        card.save()
        .then((card) => card ? card.view(true) : null)
        .then(success(res))
        .catch(next)
      })
