import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { create, charge, credit, index, show, destroy } from './controller'
import { schema } from './model'
import { Schema } from 'mongoose'
export Card, { schema } from './model'

const router = new Router()
const { name, cNumber, limit } = schema.tree
const amount = { type: Number, required: true}

/**
 * @api {post} /cards Create card
 * @apiName CreateCard
 * @apiGroup Card
  * @apiParam name Card holder name.
 * @apiParam cNumber Card Number.
 * @apiParam limit Card credit limit.
 * @apiParam balance Card balance.
 * @apiSuccess {Object} card Card's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Card not found.
 */
router.post('/',
  body({ name, cNumber, limit }),
  create)

/**
 * @api {get} /cards Retrieve all cards
 * @apiName RetrieveCards
 * @apiGroup Card
 * @apiUse listParams
 * @apiSuccess {Number} count Total amount of cards.
 * @apiSuccess {Object[]} rows List of cards.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.get('/',
  query(),
  index)

/**
 * @api {get} /cards/:id Retrieve a specific card
 * @apiName RetrieveCard
 * @apiGroup Card
 * @apiSuccess {Object} card Card's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Card not found.
 */
router.get('/:id',
  show)

/**
 * @api {patch} /cards/:id/charge recharge the card
 * @apiName chanrge
 * @apiGroup Card
 * @apiParam amount charging anount.
 * @apiSuccess {Object} card Card's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Card not found.
 */
router.patch('/:id/charge',
 body({ amount }),
 charge)

 /**
 * @api {patch} /cards/:id/credit credit from the card
 * @apiName credit
 * @apiGroup Card
 * @apiParam amount current credit amount.
 * @apiSuccess {Object} card Card's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Card not found.
 */
router.patch('/:id/credit',
body({ amount }),
credit)

export default router