import mongoose, { Schema } from 'mongoose'

const cardSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  cNumber: {
    type: String,
    unique: true,
    minlength: 1,
    maxlength: 19,
    required: true
  },
  limit: {
    type: Number,
    required: true
  },
  balance: {
    type: Number,
    required: true,
    default: 0
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

cardSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      name: this.name,
      cNumber: this.cNumber,
      limit: '£' + this.limit,
      balance: '£' + this.balance,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('Card', cardSchema)

export const schema = model.schema
export default model
