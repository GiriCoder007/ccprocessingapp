import luhn from "fast-luhn"

export const success = (res, status) => (entity) => {
  if (entity) {
    res.status(status || 200).json(entity)
  }
  return null
}

export const notFound = (res) => (entity) => {
  if (entity) {
    return entity
  }
  res.status(404).end()
  return null
}

export const invalidCharge = (body, res) => (entity) => {
  const total = entity.balance + body.amount;
  if (total <= entity.limit) {
    return entity
  }
  res.status(400).json({error: 'Invalid amount'})
  return null
}

export const isValidCard = (res) => (entity) => {
  if (entity && luhn(entity.cNumber)) {
    return entity
  }
  res.status(400).json({error: 'Invalid card number'})
  return null
}
